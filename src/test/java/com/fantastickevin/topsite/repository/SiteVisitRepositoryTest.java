package com.fantastickevin.topsite.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fantastickevin.topsite.domain.SiteVisit;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class SiteVisitRepositoryTest {
	private final static String URL_FACEBOOK = "www.facebook.com";
	private final static String URL_GOOGLE = "www.google.com";

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private SiteVisitRepository siteVisitRepository;

	private void insertSiteVisit(Date date, String website, Integer visits) {
		SiteVisit siteVisit = new SiteVisit();
		siteVisit.setDate(date);
		siteVisit.setWebsite(website);
		siteVisit.setVisits(visits);
		entityManager.persist(siteVisit);
		entityManager.flush();
	}

	@Test
	public void whenFindByDateOrderByVisitsDescThenReturnSiteVisits() {
		insertSiteVisit(new Date(2017, 1, 1), URL_FACEBOOK, 1);
		insertSiteVisit(new Date(2017, 1, 1), URL_GOOGLE, 2);

		List<SiteVisit> results = siteVisitRepository.findByDateOrderByVisitsDesc(new Date(2017, 1, 1));

		assertThat(results.size(), equalTo(2));
		assertThat(results.get(0).getWebsite(), equalTo(URL_GOOGLE));
		assertThat(results.get(1).getWebsite(), equalTo(URL_FACEBOOK));
	}
}
