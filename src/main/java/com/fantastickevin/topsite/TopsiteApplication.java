package com.fantastickevin.topsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopSiteApplication.class, args);
	}
}
