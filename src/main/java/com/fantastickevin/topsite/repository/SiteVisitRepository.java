package com.fantastickevin.topsite.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fantastickevin.topsite.domain.SiteVisit;

public interface SiteVisitRepository extends JpaRepository<SiteVisit, Long> {
	List<SiteVisit> findByDateOrderByVisitsDesc(Date date);
}
