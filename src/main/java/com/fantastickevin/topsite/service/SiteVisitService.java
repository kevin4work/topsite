package com.fantastickevin.topsite.service;

import java.util.Date;
import java.util.List;

import com.fantastickevin.topsite.domain.SiteVisit;

public interface SiteVisitService {
	List<SiteVisit> findTop5Websites(Date date);
}
